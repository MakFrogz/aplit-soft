package com.example.makfrogz.myapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends Activity {

    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;


    TextView text1;
    TextView text2;

    String[] array = new String[9];
    int i;
    String[][] arrayButton = new String [3][3];

    Button[][] arrayBut;

    int playerX;
    int playerY;

    Random random = new Random();


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.button1);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);
        btn5 = findViewById(R.id.button5);
        btn6 = findViewById(R.id.button6);
        btn7 = findViewById(R.id.button7);
        btn8 = findViewById(R.id.button8);
        btn9 = findViewById(R.id.button9);

        text1 = findViewById(R.id.textView1);
        text2 = findViewById(R.id.textView2);

        arrayBut = new Button[][]{{btn1, btn2, btn3}, {btn4, btn5, btn6}, {btn7, btn8, btn9}};


        for (int l = 0; l < arrayBut.length; l++) {
            for (int m = 0; m < arrayBut[l].length; m++) {
                gameWithComputer(arrayBut[l][m], l, m);
            }
        }


        text1.setTextSize(40);
        text2.setTextSize(40);
        text1.setText("X: " + playerX);
        text2.setText("O: " + playerY);
    }

    @SuppressLint("SetTextI18n")
    public void check() {
        boolean flag = false;
        if (arrayButton[0][0] != null && arrayButton[0][1] != null && arrayButton[0][2] != null && arrayButton[0][0] == (arrayButton[0][1]) && arrayButton[0][0] ==(arrayButton[0][2])) {
            Toast.makeText(this, arrayButton[0][0] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][0] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }
        if (arrayButton[1][0] != null && arrayButton[1][1] != null && arrayButton[1][2] != null && arrayButton[1][0] == (arrayButton[1][1]) && arrayButton[1][0] == (arrayButton[1][2])){
            Toast.makeText(this, arrayButton[1][0] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[1][0] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }
        if (arrayButton[2][0] != null && arrayButton[2][1] != null && arrayButton[2][2] != null &&arrayButton[2][0] == (arrayButton[2][1]) && arrayButton[2][0] == (arrayButton[2][2])){
            Toast.makeText(this, arrayButton[2][0] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[2][0] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }


        if (arrayButton[0][0] != null && arrayButton[1][1] != null && arrayButton[2][2] != null &&arrayButton[0][0] == (arrayButton[1][1]) && arrayButton[0][0] ==(arrayButton[2][2])){
            Toast.makeText(this, arrayButton[0][0] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][0] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }
        if (arrayButton[0][2] != null && arrayButton[1][1] != null && arrayButton[2][0] != null &&arrayButton[0][2] == (arrayButton[1][1]) && arrayButton[0][2] == (arrayButton[2][0])){
            Toast.makeText(this, arrayButton[0][2] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][2] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }



        if (arrayButton[0][0] != null && arrayButton[1][0] != null && arrayButton[2][0] != null && arrayButton[0][0] == (arrayButton[1][0]) && arrayButton[0][0] ==(arrayButton[2][0])){
            Toast.makeText(this, arrayButton[0][0] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][0] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }
        if (arrayButton[0][1] != null && arrayButton[1][1] != null && arrayButton[2][1] != null && arrayButton[0][1] == (arrayButton[1][1]) && arrayButton[0][1] == (arrayButton[2][1])){
            Toast.makeText(this, arrayButton[0][1] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][1] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }
        if (arrayButton[0][2] != null && arrayButton[1][2] != null && arrayButton[2][2] != null && arrayButton[0][2] == (arrayButton[1][2]) && arrayButton[0][2] ==(arrayButton[2][2])){
            Toast.makeText(this, arrayButton[0][2] + " Победитель", Toast.LENGTH_SHORT).show();
            flag = true;
            if (arrayButton[0][2] == "X") {
                this.playerX++;
            }else{
                this.playerY++;
            }
        }

        if (i == 9){
            Toast.makeText(this, "Ничья", Toast.LENGTH_SHORT).show();
            flag = true;
        }

        if (flag) {
            clean();
        }
        text1.setText("X: " + playerX);
        text2.setText("O: " + playerY);
    }

    public void clean(){
        for (int j = 0; j < arrayBut.length; j++) {
            for (int k = 0; k < arrayBut[j].length; k++) {
                arrayBut[j][k].setText("");
                arrayButton[j][k] = null;
                i = 0;
                array = new String[9];
            }
        }
    }

    public void  gameWithComputer(final Button button, final int l,final int m){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (array[0] == null) {
                        button.setText("X");
                        array[0] = "X";
                        arrayButton[l][m] = "X";
                        i = 1;
                        check();
                        computer();
                    } else if (array[i - 1] == "X") {
                        computer();
                    } else if (array[i - 1] == "O"){
                        if (button.getText() == "") {
                            button.setText("X");
                            array[i] = "X";
                            arrayButton[l][m] = "X";
                            i++;
                            check();
                            computer();
                        }
                    }
            }
        });
    }

    public void computer(){
        int x = random.nextInt(3);
        int y = random.nextInt(3);
            if (arrayBut[x][y].getText() == "") {
                arrayBut[x][y].setText("O");
                array[i] = "O";
                arrayButton[x][y] = "O";
                i++;
                check();
            }else{
                computer();
            }

    }

    public void  gameWithRealPerson(final Button button, final int l,final int m){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (array[0] == null) {
                    button.setText("X");
                    array[0] = "X";
                    arrayButton[l][m] = "X";
                    i = 1;
                    check();
                } else if (array[i - 1] == "X") {
                        button.setText("O");
                        array[i] = "O";
                        arrayButton[l][m] = "O";
                        i++;
                        check();
                } else if (array[i - 1] == "O"){
                    button.setText("X");
                    array[i] = "X";
                    arrayButton[l][m] = "X";
                    i++;
                    check();
                }
            }
        });
    }
}

